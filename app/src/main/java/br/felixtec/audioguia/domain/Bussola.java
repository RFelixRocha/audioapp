package br.felixtec.audioguia.domain;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.os.ResultReceiver;
import android.util.Log;
import android.widget.ImageView;

/**
 * Created by RFelix on 27/04/2017.
 */

public class Bussola  implements SensorEventListener {


    private static final int TYPE_SENSOR = Sensor.TYPE_ORIENTATION;
    private Activity mActivity;
    private Sensor mSensor;
    private SensorManager mSensorManager;
    private double angulo;
    private ImageView nav_arrow;
    private ResultReceiver mResultReceiver;



    public Bussola(Activity mActivit) {

        this.mActivity = mActivit;


        if(mActivity!=null) {
            mSensorManager =(SensorManager) mActivity.getSystemService(Context.SENSOR_SERVICE);
            mSensor = mSensorManager.getDefaultSensor(TYPE_SENSOR);

        }
    }

    public void initBussula(){

        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);

    }

    public void unregisterListenerBussola(){

        mSensorManager.unregisterListener(this);

    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if(Sensor.TYPE_ORIENTATION == event.sensor.getType()){


            if(mResultReceiver!=null){

                Bundle mBundle = new Bundle();
                mBundle.putDouble("angulo bussola: ", event.values[0]);
                mResultReceiver.send(1,mBundle);

            }

            setAngulo( event.values[0] );
            int rotation = (int) getAngulo();


            updateAngArow(rotation);


            Log.i("Bussola", "angulo: " + this.angulo);

        }

    }

    public double getAngulo() {
        return angulo;
    }

    public void setAngulo(double angulo) {
        this.angulo = angulo;
    }

    public ImageView getNav_arrow() {
        return nav_arrow;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void updateAngArow(int rotation){
        if(nav_arrow!=null)
            nav_arrow.setRotation(rotation);

    }

    public void setNav_arrow(ImageView arrow) {
        this.nav_arrow = arrow;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public ResultReceiver getmResultReceiver() {
        return mResultReceiver;
    }

    public void setmResultReceiver(ResultReceiver mResultReceiver) {
        this.mResultReceiver = mResultReceiver;
    }
}
