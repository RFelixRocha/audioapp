package br.felixtec.audioguia.util;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

/**
 * Created by RFelix on 27/04/2017.
 */

public class Bussola  implements SensorEventListener {

    private Activity mActivity;
    private SensorManager mSensorManager;
    private Sensor acelerometer;
    private Sensor magnetometer;
    private float North;
    private float newNorth;
    float[] mGravity;
    float[] mGeomagnetic;


    public Bussola( Activity mActivity) {

        this.mActivity = mActivity;
        mSensorManager = (SensorManager) mActivity.getSystemService(Context.SENSOR_SERVICE);
        acelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

    }

    public void initBussula() {

        mSensorManager.registerListener(this, acelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_NORMAL);


    }

    public void pauseBussula() {

        mSensorManager.unregisterListener(this);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {

        if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER)
            mGravity = event.values;

        if(event.sensor.getType()==Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = event.values;

        if(mGravity !=null && mGeomagnetic!=null){

            float R[] = new float[9];
            float I[] = new float [9];

            boolean sucess = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);



            float orientation[] = new float [3];
            SensorManager.getOrientation(R, orientation);
            this.North = orientation[0];



            Log.i("Bussula", "Sucess "+sucess+"[0]: "+orientation[0]+" [1]: "+orientation[1]+" [2]: "+orientation[2]);


        }


    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {


    }
    public float getNewNorth() {
        return newNorth;
    }

    public void setNewNorth(float newNorth) {
        this.newNorth = newNorth;
    }

    public float getNorth() {
        return North;
    }

    public void setNorth(float north) {
        North = north;
    }


}
