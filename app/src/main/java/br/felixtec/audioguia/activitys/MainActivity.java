package br.felixtec.audioguia.activitys;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import br.felixtec.audioguia.R;
import br.felixtec.audioguia.adapter.AdapterAudio;
import br.felixtec.audioguia.conexao.AudioHttp;
import br.felixtec.audioguia.controle.Controle;
import br.felixtec.audioguia.controle.Descricao;
import br.felixtec.audioguia.dialog.ExitDialog;
import br.felixtec.audioguia.domain.AppTalk;
import br.felixtec.audioguia.domain.Bussola;
public class MainActivity extends AppCompatActivity implements SensorEventListener,ExitDialog.ExitListener {

    int t=0;
    /*inicio posição do spinner */
    private int posicaoInicial=0;
    private String nome;
    private Descricao descricaoIni;
    private Boolean selecionou = false;
    /*fim posição do spinner*/
    //Inicios da programação Json
    int passosd;
    String upd;
    String rightd;
    String downd;
    String leftd;
    private ProgressDialog pDialog;
    Boolean posicaoFrente=true;
    private int totalPassosOposto=55;
    // lista de dados
    private List<Controle> dados;
    private AdapterAudio adapter;
    private Boolean falarQdC=false;
    private Boolean controleQtdVo=true;
    // URL para obter contatos JSON
    private static String url;
    ArrayList<HashMap<String, String>> listaAudios;
    private static String urlFrente ;//= "http://192.168.43.158/audios/frente/dadosfrente.json" ;
    private static String urlCosta;// ="http://192.168.43.158/audios/costa/dadoscosta.json";

    //novo em folha
    AudiosTask mTask;
    List<Controle> mAudios;
    TextView mTextMensagem;
    ProgressBar mProgressBar;
    ArrayAdapter<Controle> mAdapter;

    //Fim da programação Json

    /*inicio sensor passos*/
    private TextView tvTotalPassos;
    private SensorManager mSensorManager;
    private Sensor mStepDetectorSensor;
    private int totalPassos = 0;
    /*Fim sensor passos*/
    /*inicio Falar*/
    private String tag="";
    private AppTalk talk;
    /*Fim Falar*/

    /*inicio bussola*/
    private double an;
    private Bussola b;
    private double controleAng;
    /*fim bussola*/

    private String TAG = MainActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.iconebarra);


        //inicio Json
        mTextMensagem = (TextView)findViewById(android.R.id.empty);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar);

        if (mAudios == null) {
            mAudios = new ArrayList<Controle>();
        }
        mAdapter = new ArrayAdapter<Controle>(this,android.R.layout.simple_list_item_1, mAudios);

       //Fim Json
/*inicio sensor passos*/
        tvTotalPassos = (TextView) findViewById(R.id.tvTotalPassos);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mStepDetectorSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
/*Fim sensor passos*/
        /*inicio Falar*/
        talk = new AppTalk(tag, MainActivity.this);
         /*Fim Falar*/


        Intent serviceIntent = new Intent("CALLAPP");
        serviceIntent.setPackage("br.felixtec.audioguia.CallApp");
        Context context =this;
        context.startService(serviceIntent);

        /*inicio posição do listView */
        Intent it = getIntent();

        descricaoIni = it.getParcelableExtra("descricaoini");
        if(descricaoIni!= null){
            nome = String.format("sua posição inicial e no %s",descricaoIni.nome);
            posicaoInicial = descricaoIni.passos;
            totalPassos = posicaoInicial;
            posicaoFrente = descricaoIni.frente;

            //posicaoFrente = descricaoIni.frente;

            selecionou = true;
            iniciarUrl();

        }else{
            AppTalk talk = new AppTalk(MainActivity.this);
            nome = "Por favor Selecione a posição inicial";
            talk.setFala(nome);
            talk.Falar();
            selecionou = false;
        }


        /*fim posição do spinner*/


        talk = new AppTalk(tag, MainActivity.this);

        b = new Bussola(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            totalPassos =0;
            totalPassosOposto=0;
            tvTotalPassos.setText("");

             ExitDialog dialog = new ExitDialog();
             dialog.show(getFragmentManager(), "exitDialog");

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mStepDetectorSensor, SensorManager.SENSOR_DELAY_UI);

        b.initBussula();

       }

    @Override
    protected void onPause() {
        super.onPause();
        b.setmResultReceiver(new callMain(null));
        b.unregisterListenerBussola();


    }

    @Override
    protected void onStop(){
        super.onStop();

       mSensorManager.unregisterListener(this, mStepDetectorSensor);
    }

    public void iniciarUrl(){
        if(selecionou==true){



            if((posicaoFrente == true)&&(totalPassos <60)){



                Log.e(TAG,"Baixando arquivo da frente");

                if(totalPassos==1){
                    url = "http://10.0.7.161/audios/frente/dadosfrente.json";
                    //url = "http://192.168.43.158/audios/frente/dadosfrente.json";
                    iniciarDownload();
                }

                if((totalPassos==2)){
                    url = "http://10.0.7.161/audios/frente/dadosfrente1.json";
                    //totalPassos+=7;
                    iniciarDownload();
                }
                if((totalPassos==11)){
                    url = "http://10.0.7.161/audios/frente/dadosfrente2.json";
                    totalPassos+=2;
                    iniciarDownload();

                }
                if((totalPassos==23)){
                    url = "http://10.0.7.161/audios/frente/dadosfrente3.json";
                    totalPassos+=4;
                    iniciarDownload();
                }
                /*
                if((totalPassos>=26)&&(totalPassos<=27)){
                    url = "http://10.0.7.161/audios/frente/dadosfrente4.json";
                    iniciarDownload();

                }
                if((totalPassos>=29)&&(totalPassos<=30)){
                    url = "http://10.0.7.161/audios/frente/dadosfrente5.json";
                    iniciarDownload();
                }
                */
                if((totalPassos==33)){
                    url = "http://10.0.7.161/audios/frente/dadosfrente6.json";
                    //totalPassos+=7;
                    iniciarDownload();
                }
                if((totalPassos==43)){
                    url = "http://10.0.7.161/audios/frente/dadosfrente7.json";
                    totalPassos+=3;
                    iniciarDownload();
                }

                if((totalPassos==55)){
                    url = "http://10.0.7.161/audios/frente/dadosfrente8.json";
                    iniciarDownload();
                }


            }else if((posicaoFrente==false)&&(totalPassos<60)){


                Log.e(TAG,"Baixando arquivo do sentido oposto");

                if(totalPassos==1){
                    url = "http://10.0.7.161/audios/costa/dadoscosta1.json";
                    //url = "http://192.168.43.158/audios/costa/dadoscosta1.json";
                    //totalPassos+=2;
                    iniciarDownload();
                }
                if((totalPassos>=7)&&(totalPassos<=8)){
                    url = "http://10.0.7.161/audios/costa/dadoscosta2.json";
                    //url = "http://192.168.43.158/audios/costa/dadoscosta2.json";
                    //totalPassos+=5;
                    iniciarDownload();

                }
                if((totalPassos>=17)&&(totalPassos<=18)){

                    url = "http://10.0.7.161/audios/costa/dadoscosta3.json";
                    //url = "http://192.168.43.158/audios/costa/dadoscosta3.json";
                    //totalPassos+=7;
                    iniciarDownload();

                }
                /*
                if((totalPassos>=20)&&(totalPassos<=21)){
                    url = "http://10.0.7.161/audios/costa/dadoscosta4.json";
                    //url = "http://192.168.43.158/audios/costa/dadoscosta4.json";
                    iniciarDownload();

                }
                if((totalPassos>=24)&&(totalPassos<=25)){
                    url = "http://10.0.7.161/audios/costa/dadoscosta5.json";
                    //url = "http://192.168.43.158/audios/costa/dadoscosta5.json";
                    iniciarDownload();

                }
                */
                if((totalPassos>=26)&&(totalPassos<=27)){
                    url = "http://10.0.7.161/audios/costa/dadoscosta6.json";
                    //url = "http://192.168.43.158/audios/costa/dadoscosta6.json";
                    totalPassos+=5;
                    iniciarDownload();

                }

                if((totalPassos>=37)&&(totalPassos<=38)){
                    url = "http://10.0.7.161/audios/costa/dadoscosta7.json";
                   // url = "http://192.168.43.158/audios/costa/dadoscosta7.json";
                    //totalPassos+=6;
                    iniciarDownload();

                }

                if((totalPassos>=48)&&(totalPassos<=49)){
                   url = "http://10.0.7.161/audios/costa/dadoscosta8.json";
                   // url = "http://192.168.43.158/audios/costa/dadoscosta8.json";
                    //totalPassos+=5;
                    iniciarDownload();
                }


                if((totalPassos>=55)&&(totalPassos<=56)){
                   url = "http://10.0.7.161/audios/costa/dadoscosta9.json";
                 //   url = "http://192.168.43.158/audios/costa/dadoscosta9.json";
                    iniciarDownload();

                }
            }
        }
    }

    //inicio Json
    /**
     * Classe de tarefa assíncrona para obter json fazendo chamada HTTP
     */

    private void exibirProgress(boolean exibir) {
        if (exibir) {
            AppTalk talk = new AppTalk(MainActivity.this);
            talk.setFala("Novo local encontrado, baixando informações");
            talk.Falar();
            mTextMensagem.setText("Baixando informações do local...");

        }
        mTextMensagem.setVisibility(exibir ? View.VISIBLE : View.GONE);
        mProgressBar.setVisibility(exibir ? View.VISIBLE : View.GONE);
    }

    public void iniciarDownload() {
        if (mTask == null ||  mTask.getStatus() != AsyncTask.Status.RUNNING) {
            new AudiosTask().execute();

        }
    }

    @Override
    public void onExit() {
        finish();
    }

    class AudiosTask extends AsyncTask<Void, Void, List<Controle>>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            exibirProgress(true);
        }

        @Override
        protected List<Controle> doInBackground(Void... strings) {

            AudioHttp sh = new AudioHttp();
            String jsonStr = sh.makeServiceCall(url);
            return AudioHttp.carregarDescricaoJson();



        }

        @Override
        protected void onPostExecute(List<Controle> descri) {
            super.onPostExecute(descri);
            exibirProgress(false);
            if (descri != null) {
                mAudios.clear();
                mAudios.addAll(descri);
                mAdapter.notifyDataSetChanged();

                for(int i=0;i<mAdapter.getCount();i++){
                    passosd = mAudios.get(i).getPassos();
                        upd = mAudios.get(i).getUp();
                        rightd = mAudios.get(i).getRight();
                        downd=mAudios.get(i).getDown();
                        leftd=mAudios.get(i).getLeft();
                    Log.e(TAG, "Teste>>>>>>>"+mAudios.get(i).getPassos());
                    Log.e(TAG, "Teste>>>>>>>"+mAudios.get(i).getUp());
                    Log.e(TAG, "Teste>>>>>>>"+mAudios.get(i).getRight());
                    Log.e(TAG, "Teste>>>>>>>"+mAudios.get(i).getDown());
                    Log.e(TAG, "Teste>>>>>>>"+mAudios.get(i).getLeft());
                    AppTalk talk = new AppTalk(MainActivity.this);
                    talk.setFala("clique nos botões para obter mais informaçao");
                    talk.Falar();
                }

            } else {
                Log.e(TAG, "Não foi possível obter o arquivo do servidor.");
                AppTalk talk = new AppTalk(MainActivity.this);
                talk.setFala("Não foi possível obter as informações. Verifique sua conexão!");
                talk.Falar();

                Toast.makeText(MainActivity.this,
                        "Não foi possível obter o arquivo do servidor. Verifique sua conexão!",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    //fim json

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor sensor = sensorEvent.sensor;

       if(sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
            // Detecção de passos
           controleAng = b.getAngulo();
           Log.e(TAG,"Angulo de acordo com o passo >>>>"+ controleAng);

            if((controleAng>=215)&&(controleAng<=350)){
                posicaoFrente=true;
            }else{
                posicaoFrente=false;
            }

            if(selecionou==true){



                totalPassos++;
                tvTotalPassos.setText(""+totalPassos);
                Log.e(TAG,String.valueOf(totalPassos));

              if((posicaoFrente == true)&&(totalPassos <60)){

                  Log.e(TAG,"Baixando arquivo da frente");

                if(totalPassos==1){
                    url = "http://10.0.7.161/audios/frente/dadosfrente.json";
                    //url = "http://192.168.43.158/audios/frente/dadosfrente.json";
                    if((controleQtdVo==true)&&(totalPassos==1)){
                        falarQdC=true;
                        controleQtdVo=false;
                        iniciarDownload();
                    }
                    if(totalPassos==1){
                        controleQtdVo=true;
                    }

                }

                if((totalPassos>=2)&&(totalPassos<=3)){
                    url = "http://10.0.7.161/audios/frente/dadosfrente1.json";
                    if((controleQtdVo==true)&&(totalPassos==2)){
                        falarQdC=true;
                        controleQtdVo=false;
                        iniciarDownload();
                    }
                    if(totalPassos==3){
                        controleQtdVo=true;
                      //  totalPassos+=7;
                    }

                }
                if((totalPassos>=11)&&(totalPassos<=12)){
                    url = "http://10.0.7.161/audios/frente/dadosfrente2.json";
                    if((controleQtdVo==true)&&(totalPassos==11)){
                        falarQdC=true;
                        controleQtdVo=false;
                        iniciarDownload();

                    }
                    if(totalPassos==12){
                        controleQtdVo=true;
                        totalPassos+=2;
                    }

                }
                if((totalPassos>=23)&&(totalPassos<=24)){
                    url = "http://10.0.7.161/audios/frente/dadosfrente3.json";
                    if((controleQtdVo==true)&&(totalPassos==23)){
                        falarQdC=true;
                        controleQtdVo=false;
                        iniciarDownload();
                    }
                    if(totalPassos==24){
                        controleQtdVo=true;
                        //totalPassos+=7;
                    }

                }

                  /*
                if((totalPassos>=26)&&(totalPassos<=27)){
                    url = "http://10.0.7.161/audios/frente/dadosfrente4.json";
                    if((controleQtdVo==true)&&(totalPassos==26)){
                        falarQdC=true;
                        controleQtdVo=false;
                        iniciarDownload();
                    }
                    if(totalPassos==27){
                        controleQtdVo=true;
                        totalPassos+=1;
                    }

                }

                if((totalPassos>=29)&&(totalPassos<=30)){
                    url = "http://10.0.7.161/audios/frente/dadosfrente5.json";
                    if((controleQtdVo==true)&&(totalPassos==39)){
                        falarQdC=true;
                        controleQtdVo=false;

                        iniciarDownload();
                    }
                    if(totalPassos==30){
                        controleQtdVo=true;
                        totalPassos+=2;
                    }
                }
                */
                if((totalPassos>=33)&&(totalPassos<=34)){
                    url = "http://10.0.7.161/audios/frente/dadosfrente6.json";
                    if((controleQtdVo==true)&&(totalPassos==33)){
                        falarQdC=true;
                        controleQtdVo=false;
                        iniciarDownload();
                    }
                    if(totalPassos==34){
                        //totalPassos+=7;
                        controleQtdVo=true;
                    }
                }
                if((totalPassos>=43)&&(totalPassos<=44)){
                    url = "http://10.0.7.161/audios/frente/dadosfrente7.json";
                    if((controleQtdVo==true)&&(totalPassos==43)){
                        falarQdC=true;
                        controleQtdVo=false;
                        iniciarDownload();
                    }
                    if(totalPassos==44){
                        //totalPassos+=7;
                        controleQtdVo=true;
                    }


                }

                  if((totalPassos>=55)&&(totalPassos<=56)){
                      url = "http://10.0.7.161/audios/frente/dadosfrente8.json";
                      if((controleQtdVo==true)&&(totalPassos==55)){
                          falarQdC=true;
                          controleQtdVo=false;
                          iniciarDownload();
                      }
                      if(totalPassos==56){
                          controleQtdVo=true;
                      }


                  }

                if((totalPassos>=57)&&(totalPassos<=58)){
                    AppTalk talk = new AppTalk(MainActivity.this);
                    talk.setFala("Final do corredor, vire para o lado oposto para continuar");
                    talk.Falar();
                    totalPassos=60;
                }

                totalPassosOposto--;

              }else if((posicaoFrente==false)&&(totalPassos<60)){


                  Log.e(TAG,"Baixando arquivo do sentido oposto");

                  if(totalPassos==1){
                    url = "http://10.0.7.161/audios/costa/dadoscosta1.json";
                    //url = "http://192.168.43.158/audios/costa/dadoscosta1.json";
                    if((controleQtdVo==true)&&(totalPassos==1)){
                        falarQdC=true;
                        controleQtdVo=false;
                        iniciarDownload();
                    }
                    if(totalPassos==1){
                        //totalPassos=+2;
                        controleQtdVo=true;
                    }
                }
                if((totalPassos>=7)&&(totalPassos<=8)){
                    url = "http://10.0.7.161/audios/costa/dadoscosta2.json";
                    //url = "http://192.168.43.158/audios/costa/dadoscosta2.json";
                    if((controleQtdVo==true)&&(totalPassos==7)){
                        falarQdC=true;
                        controleQtdVo=false;
                        iniciarDownload();
                    }
                    if(totalPassos==8){
                        //totalPassos+=5;
                        controleQtdVo=true;
                    }
                }
                if((totalPassos>=17)&&(totalPassos<=18)){

                    url = "http://10.0.7.161/audios/costa/dadoscosta3.json";
                    //url = "http://192.168.43.158/audios/costa/dadoscosta3.json";
                    if((controleQtdVo==true)&&(totalPassos==17)){
                        falarQdC=true;
                        controleQtdVo=false;
                        iniciarDownload();
                    }
                    if(totalPassos==18){
                        //totalPassos+=7;
                        controleQtdVo=true;
                    }
                }

                /*
                if((totalPassos>=20)&&(totalPassos<=21)){
                    url = "http://10.0.7.161/audios/costa/dadoscosta4.json";
                    //url = "http://192.168.43.158/audios/costa/dadoscosta4.json";
                    if((controleQtdVo==true)&&(totalPassos==20)){
                        falarQdC=true;
                        controleQtdVo=false;
                        iniciarDownload();
                    }
                    if(totalPassos==21){
                        controleQtdVo=true;
                    }
                }
                if((totalPassos>=24)&&(totalPassos<=25)){
                    url = "http://10.0.7.161/audios/costa/dadoscosta5.json";
                    //url = "http://192.168.43.158/audios/costa/dadoscosta5.json";
                    if((controleQtdVo==true)&&(totalPassos==24)){
                        falarQdC=true;
                        controleQtdVo=false;
                        iniciarDownload();
                    }
                    if(totalPassos==25){
                        controleQtdVo=true;
                    }
                }
                */
                if((totalPassos>=26)&&(totalPassos<=27)){
                    url = "http://10.0.7.161/audios/costa/dadoscosta6.json";
                    //url = "http://192.168.43.158/audios/costa/dadoscosta6.json";
                    if((controleQtdVo==true)&&(totalPassos==26)){
                        falarQdC=true;
                        controleQtdVo=false;
                        iniciarDownload();
                    }
                    if(totalPassos==27){
                        controleQtdVo=true;
                        //totalPassos+=7;
                    }
                }

                if((totalPassos>=37)&&(totalPassos<=38)){
                    url = "http://10.0.7.161/audios/costa/dadoscosta7.json";
                    //url = "http://192.168.43.158/audios/costa/dadoscosta7.json";
                    if((controleQtdVo==true)&&(totalPassos==37)){
                        falarQdC=true;
                        controleQtdVo=false;
                        iniciarDownload();
                    }
                    if(totalPassos==38){
                        controleQtdVo=true;
                        //totalPassos+=6;
                    }
                }

                  if((totalPassos>=48)&&(totalPassos<=49)){
                      url = "http://10.0.7.161/audios/costa/dadoscosta8.json";
                      //url = "http://192.168.43.158/audios/costa/dadoscosta8.json";
                      if((controleQtdVo==true)&&(totalPassos==48)){
                          falarQdC=true;
                          controleQtdVo=false;
                          iniciarDownload();
                      }
                      if(totalPassos==49){
                          controleQtdVo=true;
                          //totalPassos+=4;
                      }
                  }


                if((totalPassos>=55)&&(totalPassos<=56)){
                    url = "http://10.0.7.161/audios/costa/dadoscosta9.json";
                    //url = "http://192.168.43.158/audios/costa/dadoscosta9.json";
                    if((controleQtdVo==true)&&(totalPassos==55)){
                        falarQdC=true;
                        controleQtdVo=false;
                        iniciarDownload();
                    }
                }

                  if((totalPassos>=57)&&(totalPassos<=60)){
                      AppTalk talk = new AppTalk(MainActivity.this);
                      talk.setFala("Final do corredor, vire para o lado oposto para continuar");
                      talk.Falar();
                      totalPassos=60;
                      controleQtdVo=false;
                  }

                totalPassosOposto++;
             }
            } else{

                if(t==0){
                AppTalk talk = new AppTalk(MainActivity.this);
                talk.setFala("Selecione uma posição inicial");
                talk.Falar();
                t=1;
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void carregarInicial(View view){
        Intent it;
        controleAng = b.getAngulo();
        Log.e(TAG,"Angulo de acordo com o passo >>>>"+ controleAng);

        if((controleAng>=215)&&(controleAng<=350)){
            it =  new Intent(this, DescricaoLocal.class);
        }else{
            it =  new Intent(this, DescricaoLocalOpostos.class);
        }

        startActivity(it);

        MainActivity.this.finish();

    }

    public void btUp (View v){
  //     posicaoFrente=true;

        if(selecionou==true){
           AppTalk talk = new AppTalk(MainActivity.this);
           talk.setFala(upd);
           talk.Falar();
        } else{
                AppTalk talk = new AppTalk(MainActivity.this);
                talk.setFala("Selecione uma posição inicial");
                talk.Falar();

            }
    }

    public void btLeft (View v){

        if(selecionou==true){
        AppTalk talk = new AppTalk(MainActivity.this);
        talk.setFala(leftd);
        talk.Falar();
        } else{
            AppTalk talk = new AppTalk(MainActivity.this);
            talk.setFala("Selecione uma posição inicial");
            talk.Falar();

       }

    }

    public void btDown (View v){
//        posicaoFrente=false;
        if(selecionou){
        AppTalk talk = new AppTalk(MainActivity.this);
        talk.setFala(downd);
        talk.Falar();
        } else{
            AppTalk talk = new AppTalk(MainActivity.this);
            talk.setFala("Selecione uma posição inicial");
            talk.Falar();

        }

    }

    public void btRight (View v){
       if(selecionou==true){
        AppTalk talk = new AppTalk(MainActivity.this);
        talk.setFala(rightd);
        talk.Falar();
       } else{
           AppTalk talk = new AppTalk(MainActivity.this);
           talk.setFala("Selecione uma posição inicial");
           talk.Falar();

       }

    }


    public void play(View v){

        controleAng = b.getAngulo();
        Log.e(TAG,"Angulo de acordo com o passo >>>>"+ controleAng);

        if(selecionou == false){
            AppTalk talk = new AppTalk(MainActivity.this);
            talk.setFala("Por favor selecione uma posição inicial e depois selecione a direção  para obter mais informações");
            talk.Falar();
        }else{
            AppTalk talk = new AppTalk(MainActivity.this);
            talk.setFala("Caminhe para obter mais informaçao sobre o local e depois click nos botões");
            talk.Falar();
        }

        }


    public class callMain extends ResultReceiver {


        public callMain(Handler handler) {
           super(handler);
        }


        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            if(resultCode==1){

                an = resultData.getDouble("angulo");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Log.i("audioguia teste","Angulo>>>>>>>>>>>>>>>>>>>>>>>>>>>>>: "+an);

                    }
                });
            }

        }
    }

}