package br.felixtec.audioguia.mService;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import br.felixtec.audioguia.activitys.OpenApp;
import br.felixtec.audioguia.domain.AppTalk;

/**
 * Created by RFelix on 27/04/2017.
 */

public class CallApp extends Service implements SensorEventListener {

    /*
    *para ter maior controle implementar o ibinder para que no momento da app estiver aberta o accelerometer
    * nao abrir outra mainactivity.
    * quando a main estiver close o servico e ativado.
    *
    */


    private  static final int ACURACIA=170;
    private static final int TIPO_SENSOR = Sensor.TYPE_ACCELEROMETER;
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private float sensorX, sensorY, sensorZ;
    private boolean activityIsClose= true;

    @Override
    public void onCreate() {
        super.onCreate();
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(TIPO_SENSOR);

        if(mSensor==null){

            AppTalk app = new AppTalk("Dispositivo nao compativel", getApplicationContext());
        }
        else{

            mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        if(mSensor==null){

            AppTalk app = new AppTalk("Dispositivo nao compativel", getApplicationContext());
        }
        else{

            mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }



        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



    @Override
    public void onSensorChanged(SensorEvent event) {

        sensorX = event.values[0];
        sensorY = event.values[1];
        sensorZ = event.values[2];

        double moviment = (sensorX*sensorX + sensorY*sensorY + sensorZ * sensorZ);


        if(moviment>=ACURACIA){
            //implemetar passar por aqui 3 vezes para evitar movimentos acidentais... :D
            Intent it = new Intent(getApplicationContext(), OpenApp.class);
            it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(it);
            activityIsClose= false;
        }

        Log.i("acelerometro", "valor de movimento: "+moviment);


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mSensorManager.unregisterListener(this);

    }


}
