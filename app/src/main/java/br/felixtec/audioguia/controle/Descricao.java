package br.felixtec.audioguia.controle;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by RFelix on 30/04/2017.
 */

public class Descricao implements Parcelable {
    public String nome;
    public int passos;
    public Boolean frente; // 0=VW;1=GM;2=Fiat;3=Ford
    public int posi;
    public String descri;

    public Descricao(String nome, int passos, Boolean frente,
                 int posi, String descri) {

        this.nome = nome;
        this.passos = passos;
        this.frente = frente;
        this.posi = posi;
        this.descri = descri;
    }

    private Descricao(Parcel in) {
        nome = in.readString();
        passos = in.readInt();
        frente = in.readByte() != 0;
        posi = in.readInt();
        descri = in.readString();
    }

    public static final Parcelable.Creator<Descricao> CREATOR = new Parcelable.Creator<Descricao>() {
        @Override
        public Descricao createFromParcel(Parcel in) {
            return new Descricao(in);
        }

        @Override
        public Descricao[] newArray(int size) {
            return new Descricao[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(nome);
        parcel.writeInt(passos);
        parcel.writeByte((byte) (frente ? 1 : 0));
        parcel.writeInt(posi);
        parcel.writeString(descri);
    }
}
