package br.felixtec.audioguia.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import br.felixtec.audioguia.R;
import br.felixtec.audioguia.domain.AppTalk;

/**
 * Created by RFelix on 27/04/2017.
 */

public class OpenApp extends AppCompatActivity {
    private Button bt;
    private static int count=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_app);
        bt = (Button)findViewById(R.id.bt);

        new AppTalk("Dê um clique longo para abrir a aplicação ou clique duas vezes para fechar", getApplicationContext()).Falar();

        bt.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                Intent it = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(it);
                count=0;
                finish();

                return false;
            }
        });

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                if(count ==2){
                    count=0;
                    finish();
                }
                else{

                    new AppTalk("Clique mais uma vez para fechar a aplicação", getApplicationContext()).Falar();

                }

            }
        });
    }



}
