package br.felixtec.audioguia.controle;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by RFelix on 26/04/2017.
 */

public class Controle implements Serializable {
public int passos;
public String up;
public String right;
public String down;
public String left;


public Controle() {
        }

public int getPassos() {
        return passos;
        }

public void setPassos(int passos) {
        this.passos = passos;
        }

public String getUp() {
        return up;
        }

public void setUp(String up) {
        this.up = up;
        }

public String getRight() {
        return right;
        }

public void setRight(String right) {
        this.right = right;
        }

public String getDown() {
        return down;
        }

public void setDown(String down) {
        this.down = down;
        }

public String getLeft() {
        return left;
        }

public void setLeft(String left) {
        this.left = left;
        }

@Override
public String toString() {
        return (String.valueOf( passos));
        }
}
