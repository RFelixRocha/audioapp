package br.felixtec.audioguia.domain;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.speech.tts.TextToSpeech;

/**
 * Created by RFelix on 27/04/2017.
 */

public class AppTalk implements  TextToSpeech.OnInitListener{

    private String fala;
    private TextToSpeech mSpeech;
    private Context com;


    public AppTalk(String fala, Context com) {
        this.fala = fala;
        this.com = com;
        mSpeech = new TextToSpeech(com, this);
    }


    public AppTalk(Context com) {

        this.com = com;
        mSpeech = new TextToSpeech(com, this);
    }

    public void setFala(String fala) {
        this.fala = fala;
    }

    public String getFala() {
        return fala;
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void Falar() {

        mSpeech.speak(getFala(), TextToSpeech.QUEUE_FLUSH, null);

    }


    @Override
    public void onInit(int status) {

        if(status == TextToSpeech.SUCCESS){

            Falar();
        }

    }
}
