package br.felixtec.audioguia.resultReceiver;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by RFelix on 27/04/2017.
 */

public class CallBussulaReceiver extends ResultReceiver {

    private double angulo;
    private TextView tv;
    public CallBussulaReceiver(Handler handler, TextView tv) {
        super(handler);
        this.tv = tv;

    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {

        if(resultCode==1){

            setAngulo(resultData.getDouble("angulo"));
            Log.i("cad", "" + getAngulo());



        }
    }


    public double getAngulo() {
        return angulo;
    }

    public void setAngulo(double angulo) {
        this.angulo = angulo;
    }
}
