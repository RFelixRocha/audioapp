package br.felixtec.audioguia.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.felixtec.audioguia.R;
import br.felixtec.audioguia.controle.Descricao;

/**
 * Created by RFelix on 05/05/2017.
 */

public class AdapterDescricaoLocal extends BaseAdapter {

    Context ctx;
    List<Descricao> descricaos;

    public AdapterDescricaoLocal(Context ctx, List<Descricao> descricaos) {
        this.ctx = ctx;
        this.descricaos = descricaos;
    }

    @Override
    public int getCount() {
        return descricaos.size();
    }

    @Override
    public Object getItem(int position) {
        return descricaos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // 1 passo
        Descricao descricao = descricaos.get(position);
        // 2 passo
        ViewHolder holder = null;
        if (convertView == null) {
            Log.d("NGVL", "View Nova => position: " + position);
            convertView = LayoutInflater.from(ctx)
                    .inflate(R.layout.item_descri_local, null);

            holder = new ViewHolder();
            //holder.imgLogo = (ImageView) convertView.findViewById(R.id.imgLogo);
            holder.txtNome = (TextView) convertView.findViewById(R.id.txtNome);
            holder.txtDescr = (TextView) convertView.findViewById(R.id.txtDescri);
            holder.txtPosi = (TextView) convertView.findViewById(R.id.txtPosicao);
            convertView.setTag(holder);
        } else {
            Log.d("NGVL", "View existente => position: "+ position);
            holder = (ViewHolder)convertView.getTag();
        }
        // 3 passo
        // 0=VW;1=GM;2=Fiat;3=Ford
        Resources res = ctx.getResources();
        TypedArray logos = res.obtainTypedArray(R.array.logos);

        //holder.imgLogo.setImageDrawable(
          //      logos.getDrawable(descricao.imagem));
        holder.txtNome.setText(descricao.nome);
        holder.txtDescr.setText(descricao.descri);
        holder.txtPosi.setText(String.valueOf(descricao.posi));
          //      (descricao.frente ? "G" : "") +
            //            (descricao.costa ? "E" : ""));
        // 4 passo
        return convertView;
    }

    static class ViewHolder {
        //ImageView imgLogo;
        TextView txtNome;
        TextView txtDescr;
        TextView txtPosi;
    }

}