package br.felixtec.audioguia.activitys;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.felixtec.audioguia.R;
import br.felixtec.audioguia.adapter.AdapterDescricaoLocal;
import br.felixtec.audioguia.controle.Descricao;
import br.felixtec.audioguia.domain.AppTalk;

public class DescricaoLocal extends AppCompatActivity {

    List<Descricao> descricaos;
    AdapterDescricaoLocal adapter;
    ListView listView;

    int i=0;
    private AppTalk talk;
    private String tag="";
    private String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descricao_local);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       listView = (ListView)findViewById(R.id.listaLocal);

        descricaos = new ArrayList<Descricao>();

            descricaos.add(new Descricao("Banheiro", 1, true,1 , "Banheiros masculino e feminino"));
            descricaos.add(new Descricao("Laboratório 312", 2, true, 2, "Laboratorio de informatica 312"));
            descricaos.add(new Descricao("Laboratório 311", 11, true, 3, "Laboratorio de informatica 311"));
            descricaos.add(new Descricao("Laboratório 309", 23, true, 4, "Laboratorio de informatica 309"));
            descricaos.add(new Descricao("Escada", 26, true, 5, "Escada de acesso ao primeiro pavimento"));
            descricaos.add(new Descricao("Laboratório 307 - CPD", 29, true, 6, "Centro de Processamento de Dados"));
            descricaos.add(new Descricao("Laboratório 305", 33, true, 7, "Laboratorio de informatica 305"));
            descricaos.add(new Descricao("Laboratorio 303", 43, true, 8, "Laboratorio de informatica 303"));
            descricaos.add(new Descricao("Laboratorio 301", 55, true, 9, "Laboratorio de informatica 301"));



        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.iconebarra);

        adapter = new AdapterDescricaoLocal(this, descricaos);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Descricao descr = (Descricao) adapterView.getItemAtPosition(position);


                Intent intent = new Intent(DescricaoLocal.this, MainActivity.class);
                intent.putExtra("descricaoini",descr);
                startActivity(intent);
                DescricaoLocal.this.finish();
                //Toast.makeText(DescricaoLocal.this,
                  //      descr.nome,Toast.LENGTH_SHORT).show();

            }
        });



        final int PADDING = 8;
        TextView txtHeader = new TextView(this);
        txtHeader.setBackgroundColor(Color.GRAY);
        txtHeader.setTextColor(Color.WHITE);
        txtHeader.setText(R.string.texto_cabecalho);
        txtHeader.setPadding(PADDING, PADDING, 0, PADDING);
        listView.addHeaderView(txtHeader);

        TextView txtFooter = new TextView(this);
        txtFooter.setText(getResources().getQuantityString(
                R.plurals.texto_rodape,
                adapter.getCount(),
                adapter.getCount()));
        txtFooter.setBackgroundColor(Color.GRAY);
        txtFooter.setGravity(Gravity.RIGHT);
        txtFooter.setPadding(0, PADDING, PADDING, PADDING);
        listView.addFooterView(txtFooter);

        // EmptyView sÃ³ funciona com ListActivity...
        listView.setEmptyView(findViewById(android.R.id.empty));


    }

 @Override
 protected void onResume() {
     super.onResume();
   for(i=0;i<adapter.getCount();i++){
         talk = new AppTalk(tag, DescricaoLocal.this);
         talk.setFala("Clique no "+ descricaos.get(i).posi+ " para selecionar o "+descricaos.get(i).nome);
         talk.Falar();
         Log.e(TAG, "Teste>>>>>>>"+descricaos.get(i).posi);
         Log.e(TAG, "Teste>>>>>>>"+descricaos.get(i).nome);
      i++;
    }
 }

}
