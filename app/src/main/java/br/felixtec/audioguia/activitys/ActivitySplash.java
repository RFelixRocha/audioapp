package br.felixtec.audioguia.activitys;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.os.ResultReceiver;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import java.util.Timer;
import java.util.TimerTask;

import br.felixtec.audioguia.R;
import br.felixtec.audioguia.domain.AppTalk;
import br.felixtec.audioguia.domain.Bussola;

public class ActivitySplash extends AppCompatActivity {

    private long atraso = 6000;
    private AppTalk talk;
    private double an,controleAng;
    private Bussola b;
    private String tag="";
    private String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        Intent serviceIntent = new Intent("CALLAPP");
        serviceIntent.setPackage("br.felixtec.audioguia.CallApp");
        Context context =this;
        context.startService(serviceIntent);

        b = new Bussola(this);


        Log.e(TAG,"Angulo de definição Splash  >>>>"+ b.getAngulo());


        talk = new AppTalk(tag, ActivitySplash.this);
        //posicaoFrente = descricaoIni.frente;
        talk.setFala("Calculando sua posição, por favor selecione uma posição inicial clicando no item da lista");
        talk.Falar();

        TimerTask task =  new TimerTask() {
            @Override
            public void run() {

                chamar();
                ActivitySplash.this.finish();

            }
        };
        Timer timer = new Timer();
        timer.schedule(task, atraso);

    }



    @Override
    protected void onResume() {
        super.onResume();

        b.initBussula();
        controleAng = b.getAngulo();
        Log.e(TAG,"Angulo onResumeSpla   >>>>"+ controleAng);
    }

    @Override
    protected void onPause() {
        super.onPause();
        b.setmResultReceiver(new ActivitySplash.callMain(null));
        b.unregisterListenerBussola();


    }

    public void chamar(){
        Intent nvLayout;
        controleAng = b.getAngulo();
        Log.e(TAG,"Angulo onResumeSpla   >>>>"+ controleAng);
        //controleAng=10;
        if((controleAng>=215)&&(controleAng<=350)){
           nvLayout = new Intent(ActivitySplash.this, DescricaoLocal.class);
        }else{
            nvLayout = new Intent(ActivitySplash.this, DescricaoLocalOpostos.class);
        }
        startActivity(nvLayout);
        ActivitySplash.this.finish();
    }

    public class callMain extends ResultReceiver {


        public callMain(Handler handler) {
            super(handler);


        }


        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            if(resultCode==1){

                an = resultData.getDouble("angulo");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Log.i("audioguia DescriçãSplas","Angulo>>>>>>>>>>>: "+an);

                    }
                });
            }

        }
    }

}