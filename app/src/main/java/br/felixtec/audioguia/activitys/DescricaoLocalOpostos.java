package br.felixtec.audioguia.activitys;
/**
 * Created by RFelix on 23/05/2017.
 */
import br.felixtec.audioguia.R;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import br.felixtec.audioguia.adapter.AdapterDescricaoLocal;
import br.felixtec.audioguia.controle.Descricao;
import br.felixtec.audioguia.domain.AppTalk;

public class DescricaoLocalOpostos extends AppCompatActivity {

    List<Descricao> descricaos;
    AdapterDescricaoLocal adapter;
    ListView listView;

    /*inicio bussola*/
    private double controleAng=120;


    int i=0;
    private AppTalk talk;
    private String tag="";
    private String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descricao_local_opostos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Log.e(TAG,"Angulo de definição de orientação  >>>>"+ b.getAngulo());
        //   controleAng = b.getAngulo();

        listView = (ListView)findViewById(R.id.listaLocal);

        descricaos = new ArrayList<Descricao>();



        descricaos.add(new Descricao("Laboratorio 301", 1, false, 1, "Laboratorio de informatica 301"));
        descricaos.add(new Descricao("Laboratorio 304", 7, false, 2, "Laboratorio de informatica 304"));
        descricaos.add(new Descricao("Laboratório 305", 17, false, 3, "Laboratorio de informatica 305"));
        descricaos.add(new Descricao("Laboratório 307 - CPD", 20, false, 4, "Centro de Processamento de Dados"));
        descricaos.add(new Descricao("Escada", 24, false, 5, "Escada de acesso ao primeiro pavimento"));
        descricaos.add(new Descricao("Laboratório 308", 26,false, 6, "Laboratorio de informatica 309"));
        descricaos.add(new Descricao("Laboratório 310", 37, false, 7, "Laboratorio de informatica 311"));
        descricaos.add(new Descricao("Laboratório 312", 48, false, 8, "Laboratorio de informatica 312"));
        descricaos.add(new Descricao("Banheiro", 55, false, 9 , "Banheiros masculino e feminino"));


        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.iconebarra);

        adapter = new AdapterDescricaoLocal(this, descricaos);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Descricao descr = (Descricao) adapterView.getItemAtPosition(position);


                Intent intent = new Intent(DescricaoLocalOpostos.this, MainActivity.class);
                intent.putExtra("descricaoini",descr);
                startActivity(intent);
                DescricaoLocalOpostos.this.finish();
                //Toast.makeText(DescricaoLocal.this,
                //      descr.nome,Toast.LENGTH_SHORT).show();

            }
        });



        final int PADDING = 8;
        TextView txtHeader = new TextView(this);
        txtHeader.setBackgroundColor(Color.GRAY);
        txtHeader.setTextColor(Color.WHITE);
        txtHeader.setText(R.string.texto_cabecalho);
        txtHeader.setPadding(PADDING, PADDING, 0, PADDING);
        listView.addHeaderView(txtHeader);

        TextView txtFooter = new TextView(this);
        txtFooter.setText(getResources().getQuantityString(
                R.plurals.texto_rodape,
                adapter.getCount(),
                adapter.getCount()));
        txtFooter.setBackgroundColor(Color.GRAY);
        txtFooter.setGravity(Gravity.RIGHT);
        txtFooter.setPadding(0, PADDING, PADDING, PADDING);
        listView.addFooterView(txtFooter);

        // EmptyView sÃ³ funciona com ListActivity...
        listView.setEmptyView(findViewById(android.R.id.empty));


    }

    @Override
    protected void onResume() {
        super.onResume();
        for(i=0;i<adapter.getCount();i++){
            talk = new AppTalk(tag, DescricaoLocalOpostos.this);
            talk.setFala("Clique no "+ descricaos.get(i).posi+ " para selecionar o "+descricaos.get(i).nome);
            talk.Falar();
            Log.e(TAG, "Teste>>>>>>>"+descricaos.get(i).posi);
            Log.e(TAG, "Teste>>>>>>>"+descricaos.get(i).nome);
            i++;
        }
    }

}

