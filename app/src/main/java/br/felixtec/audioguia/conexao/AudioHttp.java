package br.felixtec.audioguia.conexao;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import br.felixtec.audioguia.controle.Controle;

/**
 * Created by RFelix on 26/04/2017.
 */

public class AudioHttp {
    // public static final String DESCRICAO_URL_JSON =
    //       "http://192.168.43.158/audios/dadosTeste.json";
    public static String DESCRICAO_URL_JSON;

    public String makeServiceCall(String reqUrl) {
        String response = null;
        DESCRICAO_URL_JSON = reqUrl;
        return response;
    }

    private static HttpURLConnection connectar(String urlArquivo) throws IOException {
        final int SEGUNDOS = 1000;
        URL url = new URL(urlArquivo);
        HttpURLConnection conexao = (HttpURLConnection)url.openConnection();
        conexao.setReadTimeout(10 * SEGUNDOS);
        conexao.setConnectTimeout(15 * SEGUNDOS);
        conexao.setRequestMethod("GET");
        conexao.setDoInput(true);
        conexao.setDoOutput(false);
        conexao.connect();
        return conexao;
    }

    public static List<Controle> carregarDescricaoJson() {
        try {
            HttpURLConnection conexao = connectar(DESCRICAO_URL_JSON);

            int resposta = conexao.getResponseCode();
            if (resposta ==  HttpURLConnection.HTTP_OK) {
                InputStream is = conexao.getInputStream();
                JSONObject json = new JSONObject(bytesParaString(is));
                return lerJsonDescricao(json);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }




        return null;
    }

    public static List<Controle> lerJsonDescricao(JSONObject json) throws JSONException {
        List<Controle> listaDeInfo = new ArrayList<Controle>();

        JSONArray jsonDados = json.getJSONArray("Dados");
        for (int j = 0; j < jsonDados.length(); j++) {
            JSONObject jsonDado = jsonDados.getJSONObject(j);

            Controle info = new Controle();

            info.setPassos(jsonDado.getInt("passos"));
            info.setUp(jsonDado.getString("up"));
            info.setRight(jsonDado.getString("right"));
            info.setDown(jsonDado.getString("down"));
            info.setLeft(jsonDado.getString("left"));

            listaDeInfo.add(info);
        }


        return listaDeInfo;
    }

    private static String bytesParaString(InputStream is) throws IOException {
        byte[] buffer = new byte[1024];
        // O bufferzao vai armazenar todos os bytes lidos
        ByteArrayOutputStream bufferzao = new ByteArrayOutputStream();
        // precisamos saber quantos bytes foram lidos
        int bytesLidos;
        // Vamos lendo de 1KB por vez...
        while ((bytesLidos = is.read(buffer)) != -1) {
            // copiando a quantidade de bytes lidos do buffer para o bufferzÃ£o
            bufferzao.write(buffer, 0, bytesLidos);
        }
        return new String(bufferzao.toByteArray(), "UTF-8");
    }

}