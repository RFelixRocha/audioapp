package br.felixtec.audioguia.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.felixtec.audioguia.R;
import br.felixtec.audioguia.controle.Descricao;

/**
 * Created by RFelix on 30/04/2017.
 */

public class DescricaoAdapter extends BaseAdapter {

    Context ctx;
    List<Descricao> descricaos;

    public DescricaoAdapter(Context ctx, List<Descricao> descricaos) {
        this.ctx = ctx;
        this.descricaos = descricaos;
    }

    @Override
    public int getCount() {
        return descricaos.size();
    }

    @Override
    public Object getItem(int position) {
        return descricaos.get(position);
    }

    @Override
    public long getItemId(int passos) {
        return descricaos.get(passos).passos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // 1 passo
        Descricao descricao = descricaos.get(position);
        // 2 passo
        ViewHolder holder = null;
        if (convertView == null) {
            Log.d("NGVL", "View Nova => position: " + position);
            convertView = LayoutInflater.from(ctx)
                    .inflate(R.layout.item_locais, null);

            holder = new ViewHolder();
            holder.txtNome = (TextView) convertView.findViewById(R.id.txtNome);
            convertView.setTag(holder);
        } else {
            Log.d("NGVL", "View existente => position: "+ position);
            holder = (ViewHolder)convertView.getTag();
        }
        // 3 passo

        holder.txtNome.setText(descricao.nome);

        // 4 passo
        return convertView;
    }

    static class ViewHolder {

        TextView txtNome;
    }

}