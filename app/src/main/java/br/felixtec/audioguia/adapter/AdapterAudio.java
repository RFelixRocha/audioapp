package br.felixtec.audioguia.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.felixtec.audioguia.controle.Controle;

/**
 * Created by RFelix on 26/04/2017.
 */

public class AdapterAudio extends BaseAdapter {

    Context ctx;
    List<Controle> descricaos;

    public AdapterAudio(Context ctx, List<Controle> descricaos) {
        this.ctx = ctx;
        this.descricaos = descricaos;
    }

    @Override
    public int getCount() {
        return descricaos.size();
    }

    @Override
    public Object getItem(int position) {
        return descricaos.get(position);
    }

    @Override
    public long getItemId(int passos) {
        return descricaos.get(passos).passos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // 1 passo
        Controle descricao = descricaos.get(position);
        // 2 passo
        ViewHolder holder = null;
        if (convertView == null) {
            Log.d("NGVL", "View Nova => position: " + position);
            //convertView = LayoutInflater.from(ctx).inflate(R.layout.item_locais, null);

            holder = new ViewHolder();
            //    holder.txtNome = (TextView) convertView.findViewById(R.id.txtNome);
            convertView.setTag(holder);
        } else {
            Log.d("NGVL", "View existente => position: "+ position);
            holder = (ViewHolder)convertView.getTag();
        }
        // 3 passo

        //  holder.txtNome.setText(descricao.nome);

        // 4 passo
        return convertView;
    }

    static class ViewHolder {

        TextView txtNome;
    }

}
